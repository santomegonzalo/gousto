## Gousto - FrontEnd React Test

### What includes
This test was made with react-create-app to make focus on the solution and no on how to implement webpack.

### How works
I decided to not use **react-router** because I don't like their approach, so I created my own router (super simple) using **page.js**. This router support going back and fordwards without any problem.
When `Categories.jsx` and `Products.jsx` are mounted, both components will dispatch an event to call two different endpoints. Both components are independent but internally could have a relation, like which products to show dependending on the selected Category.
Because I'm using React 16, the method `componentWillReceiveProps` is not implemented inside `Product.jsx`. That's why the `filter` was done dudring `render`.

### How to run it
I'm using node@8.11.3 on my machines to support the latest `eslint`.

- `yarn install`
- `yarn test`: run all the tests
- `yarn start`: start webpack server to serve the files on debug mode using `.map`
- `yarn build`: generates all the build files into `build` folder


### Folder architecture
- `build`: includes final files including `html` `minified js` and `fonts`
- `src`: includes all of our javascript logic
  - `assets`: folder to save our icons, images or config scss files
    - `scss`: folder where I have my scss config files like colors, font names, and variables
  - `components`: folder to include generic components that we could reuse everywhere inside our app. Every component follows this structure: `__tests__` folder, `index.js` where the component lives and `styles.modules.scss` CSS Module for the component
  - `locales`: folder to include all different languages
  - `mock`: folder where I created my own mocks
  - `modules`: folder to include every section that needs a connection to redux, using the following structure: `components`, `pages` and `reducers`
  - `redux`: folder to include the initialization of redux
  - `utils`: folder to include all needed helpers
