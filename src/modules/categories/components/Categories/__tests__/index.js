import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import { Categories } from '../index';

describe('Categories', () => {
  const categories = [{
    id: 'faeedf8a-bf7d-11e5-a0f9-02fada0dd3b9',
    title: 'Drinks Cabinet',
    box_limit: 7,
    is_default: false,
    recently_added: false,
    hidden: false,
  }, {
    id: '529ea59e-bf7e-11e5-840e-02fada0dd3b9',
    title: 'Kitchenware',
    box_limit: 6,
    is_default: false,
    recently_added: false,
    hidden: false,
  }];

  const mapTestId = {
    loading: '[data-testid="CategoriesLoading"]',
  };

  it('compare with snapshots', () => {
    expect(shallow(<Categories dispatchLoadCategories={() => {}} />)).toMatchSnapshot();
    expect(shallow(<Categories categories={categories} isLoading dispatchLoadCategories={() => {}} />)).toMatchSnapshot();
    expect(shallow(<Categories categories={categories} isLoading={false} dispatchLoadCategories={() => {}} />)).toMatchSnapshot();
  });

  it('renders without crashing', () => {
    ReactDOM.render(<Categories dispatchLoadCategories={() => {}} />, document.createElement('div'));
    ReactDOM.render(<Categories categories={categories} isLoading dispatchLoadCategories={() => {}} />, document.createElement('div'));
    ReactDOM.render(<Categories categories={categories} isLoading={false} dispatchLoadCategories={() => {}} />, document.createElement('div'));
  });

  it('call load Categories', () => {
    const dispatchLoadCategories = sinon.spy();

    shallow(<Categories dispatchLoadCategories={dispatchLoadCategories} />);

    expect(dispatchLoadCategories.calledOnce).toEqual(true);
    expect(dispatchLoadCategories.calledTwice).toEqual(false);
  });

  it('show loading', () => {
    const wrapper = shallow(<Categories dispatchLoadCategories={() => {}} isLoading />);

    expect(wrapper.find(mapTestId.loading)).toExist();
  });

  it('hide loading', () => {
    const wrapper = shallow(<Categories dispatchLoadCategories={() => {}} isLoading={false} />);

    expect(wrapper.find(mapTestId.loading)).not.toExist();
  });

  it('render categories', () => {
    const wrapper = shallow(<Categories dispatchLoadCategories={() => {}} isLoading={false} categories={categories} />);

    expect(wrapper.find('Category').length).toEqual(categories.length);
  });
});
