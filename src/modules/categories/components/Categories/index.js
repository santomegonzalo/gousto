import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { loadCategories } from '../../reducers/actions';

import Category from '../Category';

import styles from './styles.module.css';

export class Categories extends PureComponent {
  componentDidMount() {
    const { dispatchLoadCategories } = this.props;

    dispatchLoadCategories();
  }

  render() {
    const { categories, isLoading, selectedCategoryId } = this.props;

    return (
      <section className={styles.Categories}>
        {
          isLoading && (
            <p data-testid="CategoriesLoading">
              loading categories...
            </p>
          )
        }
        {
          categories.map(category => (
            <Category
              key={category.id}
              name={category.title}
              id={category.id}
              selected={selectedCategoryId === category.id}
            />
          ))
        }
      </section>
    );
  }
}

Categories.defaultProps = {
  isLoading: true,
  categories: [],
};

Categories.propTypes = {
  selectedCategoryId: PropTypes.string,
  isLoading: PropTypes.bool,
  categories: PropTypes.array,
  dispatchLoadCategories: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
  dispatchLoadCategories: () => dispatch(loadCategories()),
});

const mapStateToProps = ({ categories }) => ({
  categories: categories.list,
  isLoading: categories.isLoading,
});

export default connect(mapStateToProps, mapDispatchToProps)(Categories);
