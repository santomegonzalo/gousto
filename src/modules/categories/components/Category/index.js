import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Link } from 'lib/router';

import styles from './styles.module.css';

class Category extends PureComponent {
  render() {
    const { name, selected, id } = this.props;
    const categoryClass = [styles.Category];
    let url = '';

    if (selected) {
      categoryClass.push(styles.CategorySelected);
      url = '/categories';
    } else {
      url = `/categories/${id}`;
    }

    return (
      <Link className={categoryClass.join(' ')} to={url}>
        {name}
      </Link>
    );
  }
}

Category.defaultProps = {
  selected: false,
};

Category.propTypes = {
  name: PropTypes.string,
  selected: PropTypes.bool,
  id: PropTypes.string,
};

export default Category;
