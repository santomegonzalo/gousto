import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

import Category from '../index';

describe('Category', () => {
  it('compare with snapshots', () => {
    expect(shallow(<Category />)).toMatchSnapshot();
    expect(shallow(<Category name="Foo" />)).toMatchSnapshot();
    expect(shallow(<Category name="Foo" selected />)).toMatchSnapshot();
  });

  it('renders without crashing', () => {
    ReactDOM.render(<Category />, document.createElement('div'));
    ReactDOM.render(<Category name="Foo" />, document.createElement('div'));
    ReactDOM.render(<Category name="Foo" selected />, document.createElement('div'));
  });
});
