import { get, API } from 'utils/api';

import { CATEGORIES_LOADED, CATEGORIES_LOADING } from './actionTypes';

const categoriesLoaded = categories => ({
  type: CATEGORIES_LOADED,
  data: {
    categories,
  },
});

const setLoadingStatus = isLoading => ({
  type: CATEGORIES_LOADING,
  data: {
    isLoading,
  },
});

export const loadCategories = () => (
  async (dispatch) => {
    dispatch(setLoadingStatus(true));

    const response = await get(API.categories());

    dispatch(categoriesLoaded(response.data.data));
  }
);
