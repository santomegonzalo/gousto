import { CATEGORIES_LOADED, CATEGORIES_LOADING } from './actionTypes';

const initialState = {
  list: [],
  selectedCategory: null,
  isLoading: true,
};

export default function categories(state = initialState, action = {}) {
  switch (action.type) {
    case CATEGORIES_LOADING: {
      return {
        ...state,
        isLoading: action.data.isLoading,
      };
    }
    case CATEGORIES_LOADED: {
      return {
        ...state,
        list: action.data.categories,
        isLoading: false,
      };
    }
    default: {
      return state;
    }
  }
}
