import React from 'react';
import PropTypes from 'prop-types';

import Categories from '../components/Categories';
import Products from '../../products/components/Products';

const CategoriesPage = ({ params }) => (
  <React.Fragment>
    <Categories selectedCategoryId={params.categoryId} />
    <Products selectedProductId={params.productId} selectedCategoryId={params.categoryId} />
  </React.Fragment>
);

CategoriesPage.propTypes = {
  params: PropTypes.object,
};

export default CategoriesPage;
