import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

import Product from '../index';

describe('Product', () => {
  const product = { "id": "df0ddd72-beb3-11e5-8432-02fada0dd3b9", "sku": "AP-1FCD-DES-01-P", "title": "Pots & Co - Little Pot of Chocolate", "description": "The perfect midweek gluten free treat! This little pot of luxurious chocolate ganache is crafted with the finest 70% Belgian chocolate. ", "list_price": "1.10", "is_vatable": true, "is_for_sale": true, "age_restricted": false, "box_limit": 2, "always_on_menu": false, "created_at": "2016-01-20T14:30:00+0000", "categories": [{ "id": "fec10d0e-bf7d-11e5-90a9-02fada0dd3b9", "title": "Desserts", "box_limit": 2, "is_default": false, "recently_added": false, "hidden": false, "pivot": { "created_at": "2016-08-17T13:49:27+0100" } }, { "id": "8302b6ae-3854-11e6-9e42-06f9522b85fb", "title": "Little Pot", "box_limit": 2, "is_default": false, "recently_added": false, "hidden": true, "pivot": { "created_at": "2016-08-17T13:49:27+0100" } }], "attributes": [{ "id": "c29e7cb6-f5a2-11e5-94d1-02216daf9ab9", "title": "Weight", "unit": "g", "value": "50" }, { "id": "670f0e7c-c417-11e5-95fb-02fada0dd3b9", "title": "Allergen", "unit": null, "value": " milk, soya" }], "tags": [], "images": { "365": { "src": "https://production-media.gousto.co.uk/cms/product-image-landscape/ChocolatePots_10742-2-x400.jpg", "url": "https://production-media.gousto.co.uk/cms/product-image-landscape/ChocolatePots_10742-2-x400.jpg", "width": 400 } } }; // eslint-disable-line
  const mapTestId = {
    title: '[data-testid="ProductTitle"]',
    description: '[data-testid="ProductDescription"]',
  };

  it('compare with snapshots', () => {
    expect(shallow(<Product />)).toMatchSnapshot();
    expect(shallow(<Product product={product} />)).toMatchSnapshot();
    expect(shallow(<Product product={product} isSelected />)).toMatchSnapshot();
  });

  it('renders without crashing', () => {
    ReactDOM.render(<Product />, document.createElement('div'));
    ReactDOM.render(<Product product={product} />, document.createElement('div'));
    ReactDOM.render(<Product product={product} isSelected />, document.createElement('div'));
  });

  it('exact title', () => {
    const wrapper = shallow(<Product product={product} />);

    expect(wrapper.find(mapTestId.title)).toIncludeText(product.title);
  });

  it('no description', () => {
    const wrapper = shallow(<Product product={product} />);

    expect(wrapper.find(mapTestId.description)).not.toExist();
  });

  it('with description', () => {
    const wrapper = shallow(<Product product={product} />);

    expect(wrapper.find(mapTestId.description)).not.toExist();
    wrapper.setState({ expanded: true });
    expect(wrapper.find(mapTestId.description)).toExist();
  });
});
