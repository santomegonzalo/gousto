import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import styles from './styles.module.css';

class Product extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      expanded: props.isSelected,
    };
  }

  handleChangeDescription = () => {
    const { expanded } = this.state;

    this.setState({
      expanded: !expanded,
    });
  };

  render() {
    const { product, isSelected } = this.props;
    const { expanded } = this.state;

    const classProduct = [styles.Product];

    if (!product) {
      return null;
    }

    if (isSelected) {
      classProduct.push(styles.ProductSelected);
    }

    return (
      <div className={classProduct.join(' ')}>
        <button type="button" className={styles.ProductName} data-testid="ProductTitle" onClick={this.handleChangeDescription}>
          {product.title}
        </button>
        {
          expanded && (
            <div className={styles.ProductDescription} data-testid="ProductDescription">
              {product.description}
            </div>
          )
        }
      </div>
    );
  }
}

Product.defaultProps = {
  isSelected: false,
};

Product.propTypes = {
  isSelected: PropTypes.bool,
  product: PropTypes.object,
};

export default Product;
