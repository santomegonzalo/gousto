import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { selectorGetProducts } from 'selectors/products';
import { loadProducts } from '../../reducers/actions';

import InputSearch from 'components/InputSearch';
import Product from '../Product';

export class Products extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      searchText: null,
    };
  }

  componentDidMount() {
    const { dispatchLoadProducts } = this.props;

    dispatchLoadProducts();
  }

  handleSearch = (text) => {
    if (text.length === 0) {
      this.setState({
        searchText: null,
      });
    } else {
      this.setState({
        searchText: text,
      });
    }
  };

  render() {
    const {
      products,
      selectedProductId,
      selectedCategoryId,
      isLoading,
    } = this.props;
    const { searchText } = this.state;
    let productList = products;

    if (searchText) {
      productList = products.filter(p => p.title.toLowerCase().indexOf(searchText) >= 0);
    }

    if (isLoading) {
      return (
        <p data-testid="ProductsLoading">
          loading products...
        </p>
      );
    }

    return (
      <div>
        <InputSearch onChange={this.handleSearch} />
        {
          productList.length === 0 && (
            <p data-testid="ProductsEmpty">
              There are no products to show on the selected category.
            </p>
          )
        }
        {
          productList.map(product => (
            <Product
              key={product.id}
              product={product}
              categoryId={selectedCategoryId}
              isSelected={product.id === selectedProductId}
            />
          ))
        }
      </div>
    );
  }
}

Products.defaultProps = {
  products: [],
  isLoading: true,
};

Products.propTypes = {
  products: PropTypes.array,
  selectedCategoryId: PropTypes.string,
  selectedProductId: PropTypes.string,
  isLoading: PropTypes.bool,
  dispatchLoadProducts: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
  dispatchLoadProducts: () => dispatch(loadProducts()),
});

const mapStateToProps = ({ products }, prop) => ({
  products: selectorGetProducts(products.list, prop.selectedCategoryId),
  isLoading: products.isLoading,
});

export default connect(mapStateToProps, mapDispatchToProps)(Products);
