import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import { Products } from '../index';

describe('Products', () => {
  const products = [{ "id": "df19edba-beb3-11e5-ab47-02fada0dd3b9", "sku": "AP-1FCD-DES-02-P", "title": "Pots & Co - Little Pot of Salted Caramel", "description": "When you add a touch of salt to caramel, something delicious and slightly dangerous happens. We’re championing that danger in the name of these gluten free pots of pure chocolate with salted caramel JOY.", "list_price": "1.10", "is_vatable": true, "is_for_sale": true, "age_restricted": false, "box_limit": 2, "always_on_menu": false, "created_at": "2016-01-20T14:30:00+0000", "categories": [{ "id": "fec10d0e-bf7d-11e5-90a9-02fada0dd3b9", "title": "Desserts", "box_limit": 2, "is_default": false, "recently_added": false, "hidden": false, "pivot": { "created_at": "2016-08-17T13:49:44+0100" } }, { "id": "8302b6ae-3854-11e6-9e42-06f9522b85fb", "title": "Little Pot", "box_limit": 2, "is_default": false, "recently_added": false, "hidden": true, "pivot": { "created_at": "2016-08-17T13:49:44+0100" } }], "attributes": [{ "id": "c29e7cb6-f5a2-11e5-94d1-02216daf9ab9", "title": "Weight", "unit": "g", "value": "50" }, { "id": "670f0e7c-c417-11e5-95fb-02fada0dd3b9", "title": "Allergen", "unit": null, "value": " soya, milk, egg" }], "tags": [], "images": { "365": { "src": "https://production-media.gousto.co.uk/cms/product-image-landscape/AP-1FCD-DES-02-P-x400.jpg", "url": "https://production-media.gousto.co.uk/cms/product-image-landscape/AP-1FCD-DES-02-P-x400.jpg", "width": 400 } } }, { "id": "df0ddd72-beb3-11e5-8432-02fada0dd3b9", "sku": "AP-1FCD-DES-01-P", "title": "Pots & Co - Little Pot of Chocolate", "description": "The perfect midweek gluten free treat! This little pot of luxurious chocolate ganache is crafted with the finest 70% Belgian chocolate. ", "list_price": "1.10", "is_vatable": true, "is_for_sale": true, "age_restricted": false, "box_limit": 2, "always_on_menu": false, "created_at": "2016-01-20T14:30:00+0000", "categories": [{ "id": "fec10d0e-bf7d-11e5-90a9-02fada0dd3b9", "title": "Desserts", "box_limit": 2, "is_default": false, "recently_added": false, "hidden": false, "pivot": { "created_at": "2016-08-17T13:49:27+0100" } }, { "id": "8302b6ae-3854-11e6-9e42-06f9522b85fb", "title": "Little Pot", "box_limit": 2, "is_default": false, "recently_added": false, "hidden": true, "pivot": { "created_at": "2016-08-17T13:49:27+0100" } }], "attributes": [{ "id": "c29e7cb6-f5a2-11e5-94d1-02216daf9ab9", "title": "Weight", "unit": "g", "value": "50" }, { "id": "670f0e7c-c417-11e5-95fb-02fada0dd3b9", "title": "Allergen", "unit": null, "value": " milk, soya" }], "tags": [], "images": { "365": { "src": "https://production-media.gousto.co.uk/cms/product-image-landscape/ChocolatePots_10742-2-x400.jpg", "url": "https://production-media.gousto.co.uk/cms/product-image-landscape/ChocolatePots_10742-2-x400.jpg", "width": 400 } } }]; // eslint-disable-line
  const selectedCategory = 'fec10d0e-bf7d-11e5-90a9-02fada0dd3b9';
  const selectedProduct = products[0].id;
  const mapTestId = {
    loading: '[data-testid="ProductsLoading"]',
    empty: '[data-testid="ProductsEmpty"]',
  };

  const dispatchLoad = () => {};

  it('compare with snapshots', () => {
    expect(shallow(<Products dispatchLoadProducts={dispatchLoad} />)).toMatchSnapshot();
    expect(shallow(<Products dispatchLoadProducts={dispatchLoad} isLoading />)).toMatchSnapshot();
    expect(shallow(<Products dispatchLoadProducts={dispatchLoad} isLoading={false} />)).toMatchSnapshot();
    expect(shallow(<Products dispatchLoadProducts={dispatchLoad} isLoading={false} products={products} />)).toMatchSnapshot();
    expect(shallow(<Products dispatchLoadProducts={dispatchLoad} isLoading={false} products={products} selectedCategoryId={selectedCategory} selectedProductId={selectedProduct} />)).toMatchSnapshot();
  });

  it('renders without crashing', () => {
    ReactDOM.render(<Products dispatchLoadProducts={dispatchLoad} />, document.createElement('div'));
    ReactDOM.render(<Products dispatchLoadProducts={dispatchLoad} isLoading />, document.createElement('div'));
    ReactDOM.render(<Products dispatchLoadProducts={dispatchLoad} isLoading={false} />, document.createElement('div'));
    ReactDOM.render(<Products dispatchLoadProducts={dispatchLoad} isLoading={false} products={products} />, document.createElement('div'));
    ReactDOM.render(<Products dispatchLoadProducts={dispatchLoad} isLoading={false} products={products} selectedCategoryId={selectedCategory} selectedProductId={selectedProduct} />, document.createElement('div'));
  });

  it('call load Categories', () => {
    const dispatchLoadProducts = sinon.spy();

    shallow(<Products dispatchLoadProducts={dispatchLoadProducts} />);

    expect(dispatchLoadProducts.calledOnce).toEqual(true);
    expect(dispatchLoadProducts.calledTwice).toEqual(false);
  });

  it('show loading', () => {
    const wrapper = shallow(<Products dispatchLoadProducts={dispatchLoad} isLoading />);

    expect(wrapper.find(mapTestId.loading)).toExist();
  });

  it('hide loading', () => {
    const wrapper = shallow(<Products dispatchLoadProducts={dispatchLoad} isLoading={false} />);

    expect(wrapper.find(mapTestId.loading)).not.toExist();
  });

  it('render without products', () => {
    const wrapper = shallow(<Products dispatchLoadProducts={dispatchLoad} isLoading={false} products={[]} />);

    expect(wrapper.find('Product')).not.toExist();
    expect(wrapper.find(mapTestId.empty)).toExist();
  });

  it('render products', () => {
    const wrapper = shallow(<Products dispatchLoadProducts={dispatchLoad} isLoading={false} products={products} />);

    expect(wrapper.find('Product').length).toEqual(products.length);
    expect(wrapper.find(mapTestId.empty)).not.toExist();
  });
});
