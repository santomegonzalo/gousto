import { PRODUCTS_LOADED, PRODUCTS_LOADING } from './actionTypes';

const initialState = {
  list: [],
  filteredList: null,
  isLoading: true,
};

export default function products(state = initialState, action = {}) {
  switch (action.type) {
    case PRODUCTS_LOADING: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case PRODUCTS_LOADED: {
      return {
        ...state,
        isLoading: false,
        list: action.data.products,
      };
    }
    default: {
      return state;
    }
  }
}
