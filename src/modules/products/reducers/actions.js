import { get, API } from 'utils/api';

import { PRODUCTS_LOADED, PRODUCTS_LOADING } from './actionTypes';

const productsLoaded = products => ({
  type: PRODUCTS_LOADED,
  data: {
    products,
  },
});

const setLoadingStatus = isLoading => ({
  type: PRODUCTS_LOADING,
  data: {
    isLoading,
  },
});

export const loadProducts = () => (
  async (dispatch) => {
    try {
      dispatch(setLoadingStatus(true));

      const response = await get(API.products());

      dispatch(productsLoaded(response.data.data));
    } catch (e) {
      console.error(e);
    }
  }
);
