import axios from 'axios';

const URL = 'https://api.gousto.co.uk/products/v2.0';

export const API = {
  categories: () => `${URL}/categories`,
  products: () => `${URL}/products?includes[]=categories&includes[]=attributes&sort=position&image_sizes[]=365&i mage_sizes[]=400&period_id=120`,
};

export const get = (url) => {
  return axios.get(url);
};
