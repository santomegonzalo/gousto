/* It will be better for performance to use reselect */

export const selectorGetProducts = (products, categoryId = null) => {
  if (!categoryId) {
    return products;
  }

  return products.filter(p => p.categories.some(c => c.id === categoryId));
};
