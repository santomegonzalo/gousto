import CategoriesPage from '../modules/categories/pages/CategoriesPage';

export const redirects = () => {
  const redirectList = [
    {
      from: '/',
      to: '/categories',
    },
  ];

  return redirectList;
};

export const routes = () => {
  return [
    {
      url: '/categories',
      component: CategoriesPage,
    },
    {
      url: '/categories/:categoryId',
      component: CategoriesPage,
    },
    {
      url: '/categories/:categoryId/products/:productId',
      component: CategoriesPage,
    },
  ];
};
