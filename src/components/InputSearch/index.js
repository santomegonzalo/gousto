import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import styles from './styles.module.css';

class InputSearch extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      text: '',
    };
  }

  handleChange = (e) => {
    const { onChange } = this.props;

    if (onChange) {
      onChange(e.target.value); // I could debounce the result
    }

    this.setState({
      text: e.target.value,
    });
  };

  render() {
    const { text } = this.state;

    return (
      <input className={styles.InputSearch} value={text} onChange={this.handleChange} />
    );
  }
}

InputSearch.propTypes = {
  onChange: PropTypes.func,
};

export default InputSearch;
