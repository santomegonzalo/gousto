import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import InputSearch from '../index';

describe('InputSearch', () => {
  it('compare with snapshots', () => {
    expect(shallow(<InputSearch />)).toMatchSnapshot();
    expect(shallow(<InputSearch onChange={() => {}} />)).toMatchSnapshot();
  });

  it('renders without crashing', () => {
    ReactDOM.render(<InputSearch />, document.createElement('div'));
  });

  it('listen change text', () => {
    const onChange = sinon.spy();

    const wrapper = shallow(<InputSearch onChange={onChange} />);

    wrapper.find('input').simulate('change', { target: { value: 'foo' } });

    expect(onChange.calledOnce).toEqual(true);
    expect(onChange.calledTwice).toEqual(false);
  });
});
