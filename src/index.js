import React from 'react';
import { Provider } from 'react-redux';

import './index.css';

import { routes, redirects } from './routes';

import store from './redux/store';
import registerServiceWorker from './registerServiceWorker';
import { RouterProvider } from './lib/router';

// ReactDOM is too big to have it on the main file, so I decided to create a chunk only with that file
import('react-dom')
  .then((ReactDOM) => {
    ReactDOM.render(
      <Provider store={store}>
        <RouterProvider routes={routes()} redirects={redirects()} />
      </Provider>,
      document.getElementById('root'),
    );
  });

registerServiceWorker();
