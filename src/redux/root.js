import { combineReducers } from 'redux';

import products from '../modules/products/reducers';
import categories from '../modules/categories/reducers';

export default combineReducers({
  products,
  categories,
});
