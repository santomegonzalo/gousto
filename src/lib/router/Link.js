import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import page from 'page';

class Link extends PureComponent {
  handleClick = (event) => {
    const { onClick, target, to } = this.props;

    if (onClick) {
      onClick(event);
    }

    if (!target && event.button === 0 && !event.defaultPrevented) {
      event.preventDefault();

      page(to);
    }
  };

  render() {
    const { to, children } = this.props;

    return (
      <a
        {...this.props}
        onClick={this.handleClick}
        href={to}
      >
        {children}
      </a>
    );
  }
}

Link.propTypes = {
  target: PropTypes.string,
  to: PropTypes.string.isRequired,
  children: PropTypes.any,
  onClick: PropTypes.func,
};

export default Link;
