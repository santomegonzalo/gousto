import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import page from 'page';

class RouterProvider extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      component: null,
    };
  }

  componentDidMount() {
    this.initializeRedirects();
    this.initializeRoutes();

    page.start();
  }

  initializeRedirects() {
    const { redirects } = this.props;

    redirects.forEach((redirect) => {
      page(redirect.from, (ctx) => {
        let urlTo = redirect.to;

        Object.keys(ctx.params).forEach((paramKey) => {
          urlTo = urlTo.replace(`:${paramKey}`, ctx.params[paramKey]);
        });

        // by documentation this is need it
        setTimeout(() => {
          page.replace(urlTo);
        }, 0);
      });
    });
  }

  initializeRoutes() {
    const { routes } = this.props;

    routes.forEach((route) => {
      const {
        url,
        component: Component,
      } = route;

      page(url, (ctx) => {
        this.setState({
          component: <Component params={ctx.params} queryString={ctx.querystring} />,
        });
      });
    });
  }

  render() {
    const { component } = this.state;

    return component;
  }
}

RouterProvider.defaultProps = {
  redirects: [],
};

RouterProvider.propTypes = {
  redirects: PropTypes.arrayOf(PropTypes.shape({
    from: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
  })),
  routes: PropTypes.arrayOf(PropTypes.shape({
    url: PropTypes.String,
    component: PropTypes.any,
  })).isRequired,
};

export default RouterProvider;
