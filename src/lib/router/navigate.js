import page from 'page';

const navigate = (to) => {
  page(to);
};

export default navigate;
